import subprocess
import os

inputs = ["key", "Joke", "Love_mom", "First_key", "someWord", "1122334455667788112233445566778811223344556677881122334455667788112233445566778811223344556677881122334455667788112233445566778811223344556677881122334455667788112233445566778811223344556677881122334455667788112233445566778811223344556677881122334455667788"]
outputs = ["Inner text of the third key", "Chukcha bought a refrigerator.\n— What do you need this fridge for? You live in Siberia.\n— To warm up during winter. Imagine the joy - it`s -40 outside and +4 in the refrigerator", "♥", "Just a simple text for teh first key", "", ""]
errors = ["", "", "", "", "Cannot find line in dictionary", "Unable to read word from user"]

testFaults = []

if(os.path.exists("./main")):

    print("Running " + str(len(inputs)) + " tests")
    print("----------------------------")
    for i in range(len(inputs)):
        p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate(input=inputs[i].encode())
        out = out.decode().strip()
        err = err.decode().strip()
        if out == outputs[i] and err == errors[i]:
            print(".", end="")
        else:
            print("F", end="")
            testFaults.append([i, inputs[i], out, outputs[i], err, errors[i]])
    print()

    if(len(testFaults) == 0): print("\nOK")

    for i in testFaults:
        print("----------------------------")
        print("Fault via exeuting test " + str(i[0]) + " with \"" + i[1] + "\" param.")
        print("Expected: (strout) \"" + i[3] + "\" , got \"" + i[2] + "\"")
        print("          (strerr) \"" + i[5] + "\" , got \"" + i[4] + "\"")

else:
    print("File \"main\" does not exists")