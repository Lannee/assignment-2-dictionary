PYC = python3
ASC = nasm
ASFLAGS = -felf64 -o

%.o: %.asm	
	@$(ASC) $(ASFLAGS) $@ $<

program: main.o lib.o dict.o
	@ld -o program main.o lib.o dict.o

clean: 
	@rm -f main.o main dict.o lib.o

test:
	@$(PYC) test.py

.PHONY: clean test
