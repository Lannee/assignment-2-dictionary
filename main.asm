global _start

%include "words.inc" 
%include "lib.inc"
%include "dict.inc"

%define BUFFER_SIZE 256

section .bss
BUFFER: resb BUFFER_SIZE 

section .rodata

readErrorMessage: db "Unable to read word from user", 0
findErrorMessage: db "Cannot find line in dictionary", 0

section .text

_start:
    mov rdi, BUFFER
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    je .read_error
    mov rdi, rax
    mov rsi, list_start
    call find_word
    test rax, rax
    je .find_error

    mov rdi, rax
    add rdi, 8   ; 8 - pointer size (quadword)
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string

    xor rdi, rdi
    jmp .exit

.read_error:
    mov rdi, readErrorMessage
    call print_error
    mov rdi, 1
    jmp .exit
.find_error:
    mov rdi, findErrorMessage
    call print_error
    mov rdi, 1
.exit:
    call print_newline
    jmp exit
