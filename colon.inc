%define last 0

%macro colon 2
%ifid %2
    %2:
%else
    %error "Second argument is not an id"
%endif
%ifstr %1
    dq last
    db %1, 0
    %define last %2
%else
    %error "First argument is not a string"
%endif
%endmacro
