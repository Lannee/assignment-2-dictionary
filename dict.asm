global find_word

%include "lib.inc"

section .text

; Ищет вхождения ключа в словаре
; Если подходящее вхождение найден, возвращает адрес начала вхождения в словарь, иначе 0.
; params: rdi - Указатель на нуль-терминированную строку
;         rsi - Указатель на начало словаря
find_word:
    test rdi, rdi
    je .ret0
.loop:
    test rsi, rsi
    je .ret0

    push rdi
    push rsi
    lea rsi, [rsi + 8]  ; 8 - pointer size (quadword)
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .found
    mov rsi, [rsi]
    jmp .loop
.found:
    mov rax, rsi
    ret
.ret0:
    xor rax, rax
    ret
