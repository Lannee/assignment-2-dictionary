global exit
global string_length
global print_error
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy 

%define EXIT_SYSCODE 60
%define READ_SYSCODE 0
%define WRITE_SYSCODE 1

%define STDIN 0
%define STDOUT 1
%define STDERR 2

section .text

; Принимает код возврата и завершает текущий процесс
; params: rdi - код возврата
exit: 
    mov rax, EXIT_SYSCODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; params: rdi - указатель на нуль-терминированную строку
; return: rax - длина строки (не включая нуль-терминатор)
string_length:
    xor rax, rax
.loop: 
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
; params: rdi - указаталь на нуль-терминированную строку
print_error:
    mov rsi, STDERR
    call printf_string
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; params: rdi - указаталь на нуль-терминированную строку
print_string:
    mov rsi, STDOUT

; Принимает указатель на нуль-терминированную строку, выводит её в переданный файловый дескриптор 
; params: rdi - указаталь на нуль-терминированную строку
;         rsi - файловый дескриптор
printf_string:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi                ; указатель на строку
    mov rdx, rax           ; длина строки
    mov rax, WRITE_SYSCODE ; syscall код
    ; mov rdi, STDOUT             ; stdout дескриптор
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
; params: rdi - код символа
print_char:
    push rdi               ; сохраняем значение в стек
    mov rax, WRITE_SYSCODE
    mov rdi, STDOUT
    mov rsi, rsp           ; используем в качестве адреса строки адрес элемента в стеке
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; params: rdi - выводимое число
print_int:
    cmp rdi, 0
    jns print_uint
    push rdi
    mov rdi, '-'           
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; params: rdi - выводимое число
print_uint:
    mov r8, 10               ; делитель
    mov r9, rsp              ; счетчик
    sub rsp, 8
    mov byte[rsp], 0
    mov rax, rdi
.loop:
    mov rdx, 0
    idiv r8
    add rdx, '0'
    shl dx, 8
    push dx            
    inc rsp
    cmp rax, 0
    je .print
    jmp .loop
.print:
    mov rdi, rsp
    push r9
    call print_string
    pop r9
    mov rsp, r9 
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; params: rdi - указатель на строку 1
;         rsi - указатель на строку 2
; result: rax - результат стравнения
string_equals:
    xor rax, rax
    xor rdx, rdx
.loop:
    mov ch, [rdi + rdx]
    cmp ch, byte[rsi + rdx]
    jne .end
    test ch, ch
    je .equals
    inc rdx
    jmp .loop
.equals:
    mov rax, 1
.end:    
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; result: rax - прочитанный символ
read_char:
    mov rax, READ_SYSCODE
    mov rdi, STDIN
    sub rsp, 8
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jle .end
    mov al, [rsp]
.end:    
    add rsp, 8
    ret 

%define SPACE_CHAR 0x20
%define TAB_CHAR 0x9
%define NL_CHAR 0xa

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; params:  rdi - адрес начала буфера
;          rsi - размер буфера
; results: rax - адрес буфера
;          rdx - длина слова
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi        ; buffer start
    dec rsi
    mov r13, rsi        ; buffer max length
    xor r14, r14        ; symbol counter
.loop:
    call read_char

    test rax, rax
    jle .end

    cmp rax, SPACE_CHAR
    je .space
    cmp rax, TAB_CHAR
    je .space
    cmp rax, NL_CHAR
    je .space 

    cmp r14, r13
    jge .ret0

    mov [r12 + r14], al
    inc r14
    jmp .loop 
.ret0:
    xor rax, rax
    jmp .exit
.space:
    cmp r14, 0
    je .loop     
.end:
    cmp r13, 0
    jl .exit  
    mov byte[r12 + r14], 0
    mov rax, r12
    mov rdx, r14
.exit:
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; params: rdi - указатель на строку
parse_uint:
    xor rdx, rdx
    xor rcx, rcx
    mov rsi, rdi
.loop:
    cmp byte[rsi + rdx], 0
    je .end
    movzx rdi, byte[rsi + rdx]
    push rdi
    push rcx
    push rdx
    push rsi
    call is_digit
    pop rsi
    pop rdx
    pop rcx
    pop rdi
    cmp rax, 1
    jne .end
    imul rcx, 10
    sub rdi, '0'
    add rcx, rdi
    inc rdx
    jmp .loop
.end:
    mov rax, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; params: rdi - указатель на строку
parse_int:
    push rdi
    cmp byte[rdi], '+'
    je .inc_rdi
    cmp byte[rdi], '-'  
    je .inc_rdi
    jmp .parse
.inc_rdi:
    inc rdi
.parse:
    call parse_uint
    pop rdi
    cmp rdx, 0
    je .end

    cmp byte[rdi], '-' 
    jne .end
    inc rdx
    neg rax
.end:    
    ret 

; params: rdi - значение
; returns: rax - 1 если цифра
;                0 если нет
is_digit:
    xor rax, rax
    sub rdi, 0x30
    cmp rdi, 0
    jl .end
    cmp rdi, 9
    jg .end
.digit:
    mov rax, 1
.end:
    ret
        

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; params: rdi - указатель на строку
;         rsi - указатель на буфер
;         rdx - длина буфера
string_copy:
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    push rax
    inc rax
    cmp rax, rdx
    jg .ret0
.loop:
    mov r8b, byte[rdi + rax - 1]
    mov byte[rsi + rax - 1], r8b
    dec rax
    cmp rax, 0
    je .end
    jmp .loop
.ret0:
    pop rax
    xor rax, rax
    ret
.end:
    pop rax
    ret
