%include "colon.inc"

section .data

colon "key", kry3
db "Inner text of the third key", 0

colon "Joke", joke
db "Chukcha bought a refrigerator.", 0xA, "— What do you need this fridge for? You live in Siberia.", 0xA, "— To warm up during winter. Imagine the joy - it`s -40 outside and +4 in the refrigerator", 0

colon "Love_mom", love_mom
db "♥", 0

colon "First_key", list_start
db "Just a simple text for teh first key", 0